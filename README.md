# CB CallBack


## Setup
* **Upload File to host:**
	incarcati mapa `cbcallback` cu toate fisierile in mapa `wp-content/themes/your-theme-name/inc/` (daca nu exista, creati-o)
* **Link file:**
	in mapa `wp-content/themes/your-theme-name/header` adaugati fisierile cu stiluri:

```php
	<head> <!-- <head> informativ, indica locul: puneti doar urmatorele doua rinduri -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/inc/cbcallback/cbcallback.css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/inc/cbcallback/cbcallback-phone_button.css">
	</head>
```

	in mapa `wp-content/themes/your-theme-name/footer` adaugati fisieruls js: (</body>)

```php
	<script rel="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/cbcallback/cbcallback.js"></script>
	</body> <!-- </body> informativ, indica locul: puneti doar rindul de sus -->
```

* **Set cbcallback_mail.php url for ajax**
	pentru a linka ajaxul cu "mail sender", indicati in `cbcallback.js`, url deplin pina la fisier ex:

```javascript
	// row 29:
	var sender_path = "http://metric.md/wp-content/themes/realproperty/inc/cbcallback/cbcbcallback_mail.php";
```


## Config

### Description
* Configurarile referitoare la logica de lucru le puteti seta in fisierul cbcallback.js
* ** Va puteti orenta usor dupa comentariu, in format:

```javascript
	//=====================================================================
	//=====================================================================

	    //----------

	//=====================================================================
	//=====================================================================
```

* Configurarile referitoare la mail sunt in fisierul cbcallback_mail.php


### cbcallback.js

* Timer:
* ** In caz ca modificati timerul, tineti cont de ordinea la intervalele prestabilite de timp.

```javascript
	// row 10:
	ar tr = {
        'scone': {
            'min': 121000,  //ms -> 2m 01s
            'max': 149000   //ms -> 2m 29s
        },
        'scone_a...
```

* Text Ajax:

```javascript
	// row 38:
    var ajax_mgs = {
    'done'          : 'In curind vom lua legatura',
    'error'         :...
```

* Text base:

```javascript
	// row 246:
    popupHeader: function(type) {
            var h;

            switch(type) {

                case 'type_1':
                    h = 'Lasa-ti un numar de telefon agentul nostru va va telefona';
                    break;

                ...
```

* Text button:

```javascript
	// row 299:
	popupContent: function(type) {
        var c;

        switch(type) {

            case 'type_1':     
                c = '

            ...
```

* Text footer:

```javascript
	// row 349:
	popupFooter: function(type) {
        var f;

        switch(type) {

            case 'type_1':
                f = '<span id="clbh_stat">';
                    f += 'Vă apreciem cu adevărat. P...
```


### cbcallback_mail.php
* Adresa postala, o puteti modifica

```php
	// row 11:
	EOMAILBODY;
		mail('example@mail.com', 'Mail from Metric.md', $mail_body);
```

## Requirements


### Mandatory requirements
* [jQuery](https://jquery.com/) v. 1.6+
* ** `ex: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>`


### Cross-domain requirements
* Scriptul pentru mail e facut pe baza de `php`, pentru a migra la un alt limbaj:
* ** o sa fie necesar de inlocuit fisierul `cbcallback_mail.php`
* ** de re-linkat fisierile css | js
* ** de modificat url pentru ajax in fisierul `cbcallback.js`